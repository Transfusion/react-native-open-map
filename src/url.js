import { Platform, Linking } from 'react-native';

export const getUrl = (app, lat, lng, title) => {
  if (app.name === 'apple-maps') {
    return `${app.prefixe}?ll=${lat},${lng}&q=${encodeURIComponent(title)}`;
  }
  if (app.name === 'google-maps') {
    // return `${app.prefixe}?q=${lat},${lng}`;
    if (isNaN(lng) || isNaN(lat)) {
        return `${app.prefixe}?q=${encodeURIComponent(title)}`;
    }
    //  "geo:0,0?q=-33.8666,151.1957(Google+Sydney)"
    // console.log(`${app.prefixe}?q=${lat},${lng}(${encodeURIComponent(title)})`);
    if (Platform.OS === 'ios') {
      return `${app.prefixe}?q=${lat},${lng}(${encodeURIComponent(title)})`;
    } else {
      console.log("ajsfiod");
      console.log( `${app.prefixe}${lat},${lng}?q=${encodeURIComponent(title)}`);
      return `${app.prefixe}${lat},${lng}?q=${encodeURIComponent(title)}`;
    }
  }
  if (app.name === 'citymapper') {
    return `${app.prefixe}directions?endcoord=${lat},${lng}&endname=${encodeURIComponent(title)}`;
  }
  if (app.name === 'uber') {
    title = `&dropoff[nickname]=${encodeURIComponent(title)}`;
    return `${app.prefixe}?action=setPickup&client_id=tDROEA84DI_9D2djgOt3L_JFCRAYTcvkB1m8RjO0&pickup=my_location&dropoff[latitude]=${lat}&dropoff[longitude]=${lng}${title}`;
  }
  if (app.name === 'lyft') {
    return `${app.prefixe}ridetype?id=lyft&destination[latitude]=${lat}&destination[longitude]=${lng}`;
  }
  if (app.name === 'transit') {
    return `${app.prefixe}directions?to=${lat},${lng}`;
  }
  if (app.name === 'waze') {
    console.log("wazed " + lat + " " + lng + " " + title);
    if (isNaN(lng) || isNaN(lat)) {
      return `https://www.waze.com/ul?q=${title}&zoom=14`
    }
    console.log("is not nan");
    console.log(`https://www.waze.com/ul?q=${title}&ll=${lat},${lng}&navigate=no&zoom=14`);
    // return `${app.prefixe}?ll=${lat},${lng}&navigate=yes`;
    return `https://www.waze.com/ul?q=${title}&ll=${lat},${lng}&navigate=no&zoom=14`
  }
  if (app.name === 'moovit') {
    return `${app.prefixe}directions?dest_lat=${lat}&dest_lon${lng}&dest_name=${encodeURIComponent(title)}`;
  }
};
